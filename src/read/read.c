/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 19:34:29 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/23 19:17:18 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

static	char	*scut(char **s, char c)
{
	char	*answer;
	int		n;
	size_t	i;

	n = 0;
	while ((*s)[n] != c && (*s)[n])
		n++;
	answer = ft_strnew(n);
	if (((*s) == NULL) || !answer)
		return (NULL);
	i = 0;
	n = 0;
	while ((*s)[n] && ((*s)[n] != c))
		answer[i++] = (*s)[n++];
	answer[i] = 0;
	*s += n - 1;
	return (answer);
}

static	t_line	*ft_pars(char *str)
{
	t_line	*lists;
	t_line	*list;
	char	*word;

	lists = NULL;
	while (*str)
	{
		while (*str == ' ')
			str++;
		list = create_line(ft_atoi(str));
		while ((*str != ' ') && (*str != '\0'))
			if (*str == ',')
			{
				str += 3;
				word = scut(&str, ' ');
				ft_alpha_up(&word);
				list->color = ft_atoi_base(word, "0123456789ABCDEF");
				free(word);
			}
			else
				str++;
		push_line(&lists, list);
	}
	return (lists);
}

t_map			*ft_read(char *str)
{
	int		file;
	char	*line;
	t_map	*map;

	map = NULL;
	file = open(str, O_RDONLY);
	while (gnl(file, &line))
	{
		push_row(&map, create_row(ft_pars(line)));
		free(line);
	}
	close(file);
	return (map);
}
