/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 13:29:33 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/23 13:49:59 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/lem_in.h"

static void		get_coordinates(t_lem **lem, t_graf **graf, char **line)
{
	char	**param;
	t_kin *room;

	if (!line)
		eeerrror();
	else if (check_char(*line, ' '))
		get_comment(lem, graf, line);
	else if (check_char(*line, '-'))
		get_connect(lem, graf, line);
}

static void		set_startend(t_graf **graf)
{
	t_rooms *room;

	push_rooms(&(*graf)->rooms, create_rooms(ft_strdup((*graf)->start)));
	room = create_rooms(ft_strdup((*graf)->end));
	unshift_rooms(&(*graf)->rooms, &room);
}

void		get_data(t_lem **lem, t_graf **graf, int file)
{
	char	*line;

	line = NULL;
	while(gnl(file, &line))
	{
		if (check_tag(lem, graf, &line, file))
			continue ;
		get_coordinates(lem, graf, &line);
	}
	set_startend(graf);
}
