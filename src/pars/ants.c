/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ants.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 13:39:51 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/23 13:40:01 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/lem_in.h"

void		get_ants(t_lem **lem, int file)
{
	int 	n;
	char	*line;

	line = NULL;
	gnl(file, &line);
	n = 0;
	if (!line)
		eeerrror();
	while (line[n])
	{
		if (!(line[n] >= '0' && line[n] <= '9'))
			eeerrror();
		n++;
	}
	(*lem)->ants = ft_atoi(line);
	free(line);
}
