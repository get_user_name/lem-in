/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mtx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 13:52:45 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/23 13:55:33 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/lem_in.h"

static int		**create_mtx(int n)
{
	int x;
	int y;
	int **answer;

	y = 0;
	answer = (int**)malloc(sizeof(int *) * (n));
	while (y < n)
	{
		x = 0;
		answer[y] = (int*)malloc(sizeof(int) * (n));
		while(x < n)
		{
			answer[y][x] = 0;
			x++;
		}
		y++;
	}
	return (answer);
}

static void		ft_fill_matrix(int ***mtx, int x, int y)
{
	(*mtx)[x][y] = 1;
	(*mtx)[y][x] = 1;
}

static int		check_position(t_rooms *rooms, char *str, int n)
{
	while (rooms)
	{
		n--;
		if (!ft_strcmp(str, rooms->name))
			return (n);
		rooms = rooms->next;
	}
	eeerrror();
	return (0); 
}

static void		set_mtx(t_lem *lem, t_graf *graf)
{
	t_rooms *rooms;
	t_kin	*kin;

	rooms = graf->rooms;
	kin = graf->kin;
	while (kin)
	{
		ft_fill_matrix(&lem->ribs,
			check_position(graf->rooms, (kin->name)[0], lem->rooms),
			check_position(graf->rooms, (kin->name)[1], lem->rooms));
		kin = kin->next;
	}
}

void			get_mtx(t_lem **lem, t_graf *graf)
{
	(*lem)->ribs = create_mtx((*lem)->rooms);
	set_mtx(*lem, graf);
}
