/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tag.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 13:42:35 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/23 13:43:03 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/lem_in.h"

static void	get_start(t_lem **lem, t_graf **graf, char *str, int file)
{
	char	*line;
	char	**param;

	line = NULL;
	if (!str && ft_strcmp(str, "##start"))
		eeerrror();
	gnl(file, &line);
	param = ft_strsplit(line, ' ');
	if (count_arr_ch(param) != 3)
		eeerrror();
	(*graf)->start = ft_strdup(param[0]);
	clear_arr_ch(&param);
}

static void	get_end(t_lem **lem, t_graf **graf, char *str, int file)
{
	char	*line;
	char	**param;

	line = NULL;
	if (!str && ft_strcmp(str, "##end"))
		eeerrror();
	gnl(file, &line);
	param = ft_strsplit(line, ' ');
	if (count_arr_ch(param) != 3)
		eeerrror();
	(*graf)->end = ft_strdup(param[0]);
	clear_arr_ch(&param);
}

int		    check_tag(t_lem **lem, t_graf **graf, char **line, int file)
{
	if (!ft_strcmp(*line, "#comment") || !ft_strcmp(*line, "#another comment"))
	{
		free(*line);
		return (1);
	}
	else if (!ft_strcmp(*line, "##start"))
	{
		if ((*graf)->start)
			eeerrror();
		get_start(lem, graf, *line, file);
		free(*line);
		return (1);
	}
	else if (!ft_strcmp(*line, "##end"))
	{
		if ((*graf)->end)
			eeerrror();
		get_end(lem, graf, *line, file);
		free(*line);
		return (1);
	}
	return (0);
}
