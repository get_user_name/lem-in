/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lim_read.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 13:19:12 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/23 13:56:46 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/lem_in.h"

void		lim_read(char *str, t_lem **lem)
{
	int		file;
	char	*line;
	t_graf	*graf;

	line = NULL;
	graf = create_graf();
	*lem = create_lem();
	file = open(str, O_RDONLY);
	get_ants(lem, file);
	free(line);
	get_data(lem, &graf, file);
	get_mtx(lem, graf);
	// ft_putconnets(graf->kin, graf->rooms);
	// ft_putendl(graf->start);
	// ft_putendl(graf->end);
	// ft_putnbr((*lem)->rooms);
	// ft_putendl("");
	close(file);
}
