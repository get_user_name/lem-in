/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 20:38:09 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/19 16:44:40 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

void	linegor(t_root *root ,int x1, int x2, int y, int color)
{
	while (x1 <= x2)
	{
		mlx_pixel_put(root->mlx_ptr, root->win_ptr, x1, y, color);
		x1++;
	}
}

void	fdf_circle(t_root *root, int r, int color)
{
	int X1 = 250;
	int	Y1 = 250;
	int x = 0;
	int y = r;
	int delta = 1 - 2 * r;
	int error = 0;
	while (y >= 0)
	{
		linegor(root, X1 - x, X1 + x, Y1 - y, color);
		linegor(root, X1 - x, X1 + x, Y1 + y, color);
		error = 2 * (delta + y) - 1;
		if ((delta < 0) && (error <= 0))
		{
			delta += 2 * ++x + 1;
			continue ;
		}
		if ((delta > 0) && (error > 0))
		{
			delta -= 2 * --y + 1;
			continue ;
		}
		delta += 2 * (++x - y--);
	}
}

void	ft_draw(t_root *root)
{
	root->mtrx = get_mtrx(root->map, root);
	run_points(root);
	fdf_circle(root, 50, 0x800080);
	paint(root, root->mtrx);

}
