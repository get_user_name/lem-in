/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 20:55:39 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/24 16:20:52 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

static	void	bzm_sum(int error, t_bzm **bzm, int *x1, int *y1)
{
	if (error > -((*bzm)->deltaY))
	{
		(*bzm)->error -= (*bzm)->deltaY;
		*x1 += (*bzm)->signX;
	}
	if (error < (*bzm)->deltaX)
	{
		(*bzm)->error += (*bzm)->deltaX;
		*y1 += (*bzm)->signY;
	}
}

static	void	min_mlx_pixel_put(t_root *root, int x, int y, int color)
{
	x += root->mtrx->x1 + root->x;
	y += root->mtrx->y1 + root->y;
	mlx_pixel_put(root->mlx_ptr, root->win_ptr, x, y, color);
}

static	void	brezenhem(t_root *root, t_strip *strip1, t_strip *strip2)
{
	int		error;
	t_bzm	*bzm;
	int		x1;
	int		y1;

	x1 = (strip1->zxy)[1];
	y1 = (strip1->zxy)[2];
	bzm = create_bzm(strip1, strip2);
	min_mlx_pixel_put(root, (strip2->zxy)[1], (strip2->zxy)[2], strip1->color);
	while ((x1 != (strip2->zxy)[1]) || (y1 != (strip2->zxy)[2]))
	{
		min_mlx_pixel_put(root, x1, y1, strip1->color);
		error = bzm->error * 2;
		if (error > -(bzm->deltaY))
		{
			bzm->error -= bzm->deltaY;
			x1 += bzm->signX;
		}
		if (error < bzm->deltaX)
		{
			bzm->error += bzm->deltaX;
			y1 += bzm->signY;
		}
	}
}

void			paint(t_root *root, t_mtrx *mtrx)
{
	t_strip *strip1;
	t_strip *strip2;

	while (mtrx)
	{
		strip1 = NULL;
		strip2 = NULL;
		strip1 = mtrx->strip;
		if (mtrx->next)
			strip2 = mtrx->next->strip;
		while (strip1)
		{
			if (strip1->next)
				brezenhem(root, strip1, strip1->next);
			if (strip2)
			{
				brezenhem(root, strip1, strip2);
				strip2 = strip2->next;
			}
			strip1 = strip1->next;
		}
		mtrx = mtrx->next;
	}
}
