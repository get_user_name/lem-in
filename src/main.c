/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 12:29:38 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/23 14:33:08 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"
#include <stdio.h> 

void	put_mtx(t_lem *lem)
{
	int x;
	int y;

	y = 0;
	while (y < lem->rooms)
	{
		x = 0;
		while(x < lem->rooms)
		{
			printf("%d ", lem->ribs[y][x]);
			x++;
		}
		printf("\n");
		y++;
	}
}

void		ft_putconnets(t_kin *kin, t_rooms *rooms)
{
	while (kin)
	{
		ft_putstr((kin->name)[0]);
		ft_putstr(" - ");
		ft_putstr((kin->name)[1]);
		ft_putstr(" // ");
		if (rooms)
		{
			ft_putstr(rooms->name);
			rooms = rooms->next;
		}
		ft_putendl("");
		kin = kin->next;
	}
}

void		lem_in(char *str, int ac)
{
	t_lem *lem;
	

	lim_read(str, &lem);
	put_mtx(lem);
}

int	 main(int ac, char **av)
{
	int		n;

	char	*test;
	n = 0;
	if (ac > 1)
		lem_in(av[1], ac);
	else
		ft_putendl("There is no argument.");
	return (0);
}
