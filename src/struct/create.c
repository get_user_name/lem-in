/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 19:25:58 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/23 19:32:54 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

t_root	*create_root(int width, int height, t_map *map, int zoom)
{
	t_root	*new;

	if (!(new = (t_root*)malloc(sizeof(t_root))))
		exit(EXIT_FAILURE);
	new->width = width;
	new->height = height;
	new->zoom = zoom;
	new->x = 0;
	new->y = 0;
	new->x_axis = 0;
	new->y_axis = 0;
	new->z_axis = 0;
	new->mlx_ptr = NULL;
	new->win_ptr = NULL;
	new->map = map;
	new->mtrx = NULL;
	return (new);
}

t_map	*create_row(t_line *line)
{
	t_map	*new;

	if (!(new = (t_map*)malloc(sizeof(t_map))))
		exit(EXIT_FAILURE);
	new->line = line;
	new->next = NULL;
	return (new);
}

t_line	*create_line(int number)
{
	t_line	*new;

	if (!(new = (t_line*)malloc(sizeof(t_line))))
		exit(EXIT_FAILURE);
	new->color = 0xFFFFFF;
	new->number = number;
	new->next = NULL;
	return (new);
}

t_strip	*create_strip(void)
{
	t_strip	*new;

	if (!(new = (t_strip*)malloc(sizeof(t_strip))))
		exit(EXIT_FAILURE);
	new->zxy = (int*)malloc(sizeof(int) * 3);
	new->color = 0xFFFFFF;
	new->next = NULL;
	return (new);
}

t_mtrx	*create_mtrx(t_strip *strip)
{
	t_mtrx	*new;

	if (!(new = (t_mtrx*)malloc(sizeof(t_mtrx))))
		exit(EXIT_FAILURE);
	new->x1 = 9999999;
	new->x2 = 0;
	new->y1 = 9999999;
	new->y2 = 0;
	new->strip = strip;
	new->next = NULL;
	return (new);
}
