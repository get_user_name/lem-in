/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   crtlem.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 13:00:52 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/22 21:59:49 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/lem_in.h"

t_lem	*create_lem()
{
	t_lem	*new;

	if (!(new = (t_lem*)malloc(sizeof(t_lem))))
		exit(EXIT_FAILURE);
	new->ribs = NULL;
	new->ants = 0;
	new->rooms = 0;
	return (new);
}

t_graf	*create_graf()
{
	t_graf	*new;

	if (!(new = (t_graf*)malloc(sizeof(t_graf))))
		exit(EXIT_FAILURE);
	new->start = NULL;
	new->end = NULL;
	new->kin = NULL;
	new->rooms = NULL;
	return (new);
}

t_kin	*create_kin(char **name)
{
	t_kin	*new;

	if (!(new = (t_kin*)malloc(sizeof(t_kin))))
		exit(EXIT_FAILURE);
	new->name = name;
	new->next = NULL;
	return (new);
}

t_rooms	*create_rooms(char *name)
{
	t_rooms	*new;

	if (!(new = (t_rooms*)malloc(sizeof(t_rooms))))
		exit(EXIT_FAILURE);
	new->name = name;
	new->next = NULL;
	return (new);
}

// t_connect	*create_connect(char *name)
// {
// 	t_connect	*new;

// 	if (!(new = (t_connect*)malloc(sizeof(t_connect))))
// 		exit(EXIT_FAILURE);
// 	new->name = name;
// 	new->next = NULL;
// 	return (new);
// }
