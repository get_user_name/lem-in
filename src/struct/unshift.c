/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   unshift.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 13:32:40 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/22 22:10:02 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/lem_in.h"

int		unshift_kin(t_kin **begin_list, t_kin **data)
{
	t_kin	*next;

	if (!*data)
		return (1);
	next = *begin_list;
	if (next)
	{
		(*data)->next = *begin_list;
		*begin_list = *data;
	}
	else
		*begin_list = *data;
	return (0);
}

int		unshift_rooms(t_rooms **begin_list, t_rooms **data)
{
	t_rooms	*next;

	if (!*data)
		return (1);
	next = *begin_list;
	if (next)
	{
		(*data)->next = *begin_list;
		*begin_list = *data;
	}
	else
		*begin_list = *data;
	return (0);
}
