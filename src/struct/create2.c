/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 19:28:20 by gkoch             #+#    #+#             */
/*   Updated: 2019/01/23 19:28:32 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/fdf.h"

t_bzm	*create_bzm(t_strip *strip1, t_strip *strip2)
{
	t_bzm	*new;

	if (!(new = (t_bzm*)malloc(sizeof(t_bzm))))
		exit(EXIT_FAILURE);
	new->deltaX = ft_abs((strip2->zxy)[1] - (strip1->zxy)[1]);
	new->deltaY = ft_abs((strip2->zxy)[2] - (strip1->zxy)[2]);
	new->signX = (strip1->zxy)[1] < (strip2->zxy)[1] ? 1 : -1;
	new->signY = (strip1->zxy)[2] < (strip2->zxy)[2] ? 1 : -1;
	new->error = new->deltaX - new->deltaY;
	return (new);
}
