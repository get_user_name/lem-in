/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/17 15:52:19 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/23 14:37:47 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include	"./libft.h"
# include	"./fdf.h"
# include	"../mlx/mlx.h"

typedef	struct			s_lem
{
	int					**ribs;
	int					ants;
	int					rooms;
}						t_lem;

typedef	struct			s_graf
{
	char				*start;
	char				*end;
	struct s_kin		*kin;
	struct s_rooms		*rooms;
}						t_graf;

typedef	struct			s_kin
{
	char				**name;
	struct s_kin		*next;
}						t_kin;

typedef	struct			s_rooms
{
	char				*name;
	struct s_rooms		*next;
}						t_rooms;

/*
-------------------------------------------------------------------
** -------------------------- READ --------------------------------
-------------------------------------------------------------------
*/
void					lim_read(char *str, t_lem **lem);

/*
** -------------------------- Tools --------------------------------
*/
int						check_char(const char *str, char i);
void					eeerrror();
void					clear_arr_ch(char ***str);
int						count_arr_ch(char **str);
/*
** -------------------------- Tools --------------------------------
*/
void					get_ants(t_lem **lem, int file);
/*
** -------------------------- DATA --------------------------------
*/
void					get_data(t_lem **lem, t_graf **graf, int file);
int		 				check_tag(t_lem **lem, t_graf **graf, char **line, int file);
void					get_connect(t_lem **lem, t_graf **graf, char **line);
void					get_comment(t_lem **lem, t_graf **graf, char **line);
/*
** -------------------------- Matrix --------------------------------
*/
void			        get_mtx(t_lem **lem, t_graf *graf);

/*
-------------------------------------------------------------------
** ------------------------- STRUCT -------------------------------
-------------------------------------------------------------------
*/
t_lem				*create_lem();
t_kin				*create_kin(char **name);
t_graf				*create_graf();
t_rooms				*create_rooms(char *name);
int					unshift_kin(t_kin **begin_list, t_kin **data);
int					unshift_rooms(t_rooms **begin_list, t_rooms **data);
int					push_rooms(t_rooms **begin_list, t_rooms *data);

#endif
