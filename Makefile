# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/23 13:40:40 by gkoch             #+#    #+#              #
#    Updated: 2019/02/23 14:38:12 by gkoch            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME = fdf

SRC = ./src/
STRUCT = struct/
PARS = pars/

LEM_IN = $(SRC)main.c \
		$(SRC)$(STRUCT)crtlem.c $(SRC)$(STRUCT)unshift.c \
		$(SRC)$(PARS)lim_read.c $(SRC)$(PARS)tools.c $(SRC)$(PARS)data.c \
			$(SRC)$(PARS)ants.c $(SRC)$(PARS)tag.c $(SRC)$(PARS)coordinates.c \
			$(SRC)$(PARS)mtx.c

DRAW = draw/
READ = read/
CONTROL = control/

FDF = $(SRC)fdf.c \
		$(SRC)$(STRUCT)create.c $(SRC)$(STRUCT)create2.c $(SRC)$(STRUCT)push.c \
		$(SRC)$(DRAW)draw.c $(SRC)$(DRAW)mtrx.c $(SRC)$(DRAW)run_points.c $(SRC)$(DRAW)print.c \
		$(SRC)$(READ)read.c \
		$(SRC)$(CONTROL)control.c \

ROOT = $(LEM_IN) $(FDF)
	
	
OBJ=$(ROOT:.c=.o)
CC = clang
CFLAGS = -g #-Wall -Wextra -Werror
LIB = libft/libft.a
LIBDIR = libft/
LIBSPATH = -I libft/ -I /usr/local/include/
HDR = fdf.h
LINK = -lmlx -framework OpenGL -framework AppKit -L /usr/local/lib/

all: lib $(OBJ) $(NAME)

$(NAME): $(LIB) $(OBJ)
	$(CC) $(CFLAGS) $(LIBSPATH) -o $(NAME) $(LINK) $(LIB) $(OBJ)

lib:
	make -C $(LIBDIR)

%.o: %.c $(HDR) $(LIB)
	$(CC) $(CFLAGS) -c $< -o $@ $(LINK)

fclean: clean
	/bin/rm -f $(NAME)
	make -C $(LIBDIR) fclean

clean: cleanlib
	/bin/rm -f $(OBJ)

cleanlib:
	make -C $(LIBDIR) clean

re: fclean all
